from django.core.management.base import BaseCommand
from enrollment.models import VideoProgressModel, EnrollmentModel


class Command(BaseCommand):
    help = 'Sync payroll record'

    def handle(self, *args, **options):
        self.stdout.write('Iniciando migracion de progreso de videos')
        self.stdout.write('---------------------------------------------------')

        for progress in VideoProgressModel.objects.all():

            course_id = progress.course_id
            user_id = progress.user_id

            try:
                enrollment = EnrollmentModel.objects.get(user_id=user_id, course_id=course_id)
                progress.enrollement_id = enrollment.id
                progress.course_category_id = enrollment.course.category_id
                progress.save()
                self.stdout.write("actualizado enrollment_id: {}".format(enrollment.id))

            except Exception as error:
                self.stdout.write("{}".format(error))

        self.stdout.write('---------------------------------------------------')
        self.stdout.write('Fin del proceso')

