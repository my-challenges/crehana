# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import (
    CourseCategoryModel,
    CourseModel,
    EnrollmentModel,
    VideoProgressModel,
)


class VideoProgressModelAdmin(admin.ModelAdmin):
    list_display = ('minutes', 'user', 'course', 'enrollement', 'course_category')


admin.site.register(CourseCategoryModel)
admin.site.register(CourseModel)
admin.site.register(EnrollmentModel)
admin.site.register(VideoProgressModel, VideoProgressModelAdmin)
