from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


class CourseCategoryModel(models.Model):

    name = models.CharField(
        verbose_name=_('Nombre'),
        max_length=128
    )

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        db_table = 'course_category'
        verbose_name = _('course category')
        verbose_name_plural = _('courses categories')


class CourseModel(models.Model):

    title = models.CharField(
        verbose_name=_('Título'),
        max_length=128
    )

    category = models.ForeignKey(
        CourseCategoryModel,
        verbose_name=_('Categoría'),
        on_delete=models.PROTECT
    )

    def __str__(self):
        return "{} - {}".format(self.title, self.category.name)

    class Meta:
        db_table = 'course'
        verbose_name = _('course')
        verbose_name_plural = _('courses')


class EnrollmentModel(models.Model):

    user = models.ForeignKey(
        User,
        verbose_name=_('Usuario Id'),
        on_delete=models.PROTECT
    )

    course = models.ForeignKey(
        CourseModel,
        verbose_name=_('Curso Id'),
        on_delete=models.PROTECT
    )

    def __str__(self):
        return "{} | {} - {}".format(self.id, self.user_id, self.course_id)

    class Meta:
        db_table = 'enrollment'
        verbose_name = _('enrollment')
        verbose_name_plural = _('enrollments')
        unique_together = (
            ('user', 'course')
        )


class VideoProgressModel(models.Model):

    minutes = models.TimeField(
        verbose_name=_('Minutos')
    )

    user = models.ForeignKey(
        User,
        verbose_name=_('Usuario Id'),
        on_delete=models.PROTECT
    )

    course = models.ForeignKey(
        CourseModel,
        verbose_name=_('Curso Id'),
        on_delete=models.PROTECT
    )

    enrollement = models.ForeignKey(
        EnrollmentModel,
        verbose_name=_('Inscripción Id'),
        on_delete=models.PROTECT,
        null=True
    )

    course_category = models.ForeignKey(
        CourseCategoryModel,
        verbose_name=_('Categoría de curso Id'),
        on_delete=models.PROTECT,
        null=True
    )

    def __str__(self):
        return "{} - {}".format(self.user_id, self.course_id)

    class Meta:
        db_table = 'video_progress'
        verbose_name = _('video progres')
        verbose_name_plural = _('videos progresses')


