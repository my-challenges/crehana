# Generated by Django 3.0.4 on 2020-03-25 13:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('enrollment', '0003_auto_20200325_1355'),
    ]

    operations = [
        migrations.AddField(
            model_name='videoprogressmodel',
            name='course_category',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='enrollment.CourseCategoryModel', verbose_name='Categoría de curso Id'),
        ),
        migrations.AddField(
            model_name='videoprogressmodel',
            name='enrollement',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='enrollment.EnrollmentModel', verbose_name='Inscripción Id'),
        ),
    ]
