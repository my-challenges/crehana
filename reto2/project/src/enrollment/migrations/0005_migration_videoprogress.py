# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations


def update_video_progress(apps, schema_editor):

    response = []
    VideoProgressModel = apps.get_model('enrollment', 'VideoProgressModel')
    EnrollmentModel = apps.get_model('enrollment', 'EnrollmentModel')

    for progress in VideoProgressModel.objects.all():
        result = {
            "error": "",
            "update": ""
        }
        course_id = progress.course_id
        user_id = progress.user_id

        try:
            enrollment = EnrollmentModel.objects.get(user_id=user_id, course_id=course_id)
            progress.enrollement_id = enrollment.id
            progress.course_category_id = enrollment.course.category_id
            progress.save()
            result.update({"update": progress.id})

        except Exception as error:
            result.update({"error": "{}".format(error)})

        response.append(result)

    return response


class Migration(migrations.Migration):

    dependencies = [
        ('enrollment', '0004_auto_20200325_1356'),
    ]

    operations = [
        migrations.RunPython(update_video_progress),
    ]
