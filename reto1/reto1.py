"""
Para una lista de x elementos, queremos encontrar los 2 primeros elementos que sumen y.
Ejemplo:
x = [1, 5, 8, 2, 9]
y = 10
"""


def get_sum(total, lista):
    complemento = {}
    response = None

    for numero in lista:
        if not complemento:
            complemento[total - numero] = numero
        else:
            nro_deseado = total - numero
            if numero in complemento:
                response = "{} - {}".format(numero, nro_deseado)
                break
            else:
                complemento[total - numero] = numero

    return response


result = get_sum(10, [1, 5, 8, 2, 9])
print(result)
